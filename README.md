# Trial extension analyzer

## Purpose

Create a report that shows trial extension durations.

### Collect data

Use Zendesk's `Advanced Search` to create the data file:
- Select `Tags is`: `ir_extend_saas_trial` + `ir_extend_sm_trial`
- Select `Columns`: `ID` + `Subject` + `Description` + `Created at`
- Run `Search`
- Select `Download CSV`

Put the resulting `ticket_exports.csv` file in the same directory as the `trial.rb` script.

### Create report

Run the `trial.rb` script.\
The script expects a `ticket_exports.csv` file in the same directory as where the script is.\
The report is output to the console.

Change the `ANONYMIZE = true` setting in the script to `ANONYMIZE = false` to show domain names in the report
instead of numbers.

### SM trial extensions report logic

The script extracts the `Contact's email` from the request and determines the domain.\
E.g. the email `jubjub@example.com` has domain `example.com`.

Three dates are extracted from the internal request: 

```
Current license expiration date: 2023-07-21
Desired expiration date: 2023-07-28
```

plus the date the internal request was created.

The difference between `Desired expiration date` and `Current license expiration date` is calculated 
and stored as extension duration for a domain.\

The current and desired expiration dates are filled in by a human.\
This is where the fun starts.

### SM report

The report looks like this:

```
jubjub.com
  35 (2023-03-14: 2023-03-17 - 2023-04-21 #385572)

bandersnatch.org
  28 (2023-01-27: 2023-01-31 - 2023-02-28 #366964)
  31 (2023-03-07: 2023-02-28 - 2023-03-31 #382565)
```

or this when `ANONYMIZED`:

```
144
  35 (2023-03-14: 2023-03-17 - 2023-04-21 #385572)

148
  28 (2023-01-27: 2023-01-31 - 2023-02-28 #366964)
  31 (2023-03-07: 2023-02-28 - 2023-03-31 #382565)
```

The trials are grouped by domain.\
Each line is a trial extension, showing `# of days (IR request date: start date - expiration date #ZDticket`.

If the total calculated duration of extensions for a domain is <= 31 days, the domain is omitted from the report,
unless there is an extension with a negative duration.

### Problematic SM data

A) The script looks at the subject of the internal request.\
If it's `IR - SM - Extend an existing trial` or `IR - SaaS - Extend an existing trial` it process the internal request.\
Any other subject, like `Re: IR - SM - Extend an existing trial`, is skipped even though it might be a valid request.

B) Requests without a contact email are skipped, because the domain can't be determined.

C) Both current and desired expiration dates should be filled, otherwise the request is skipped.

D) Sometimes a trial extension is requested while in fact it's a subscription extension.\
There's nothing the script can do about this, it counts them erroneously as a trial extension.

E) The extensions are grouped by domain.\
It's possible that multiple persons in the same domain had a trial.\
The script incorrectly groups them, resulting in a too large total extension duration. 

A human review will probably spot this, as there will be overlapping extension periods. 

F) A trial extension can be requested after the trial already expired.\
Example:

```
141
  92 (2023-03-17: 2023-02-11 - 2023-05-14 #386592)
```

The `Current license expiration` is given as `2023-02-11`, but the extension request wasn't made until `2023-03-17`.\
Assuming the given `Current license expiration` was given correctly, this means there was a gap in the trial.\
The script will correct this by not using the `Current license expiration` date, but the ticket creation date for
the extension duration calculation.\
The result is then:

```s
141
  58 (2023-03-17: 2023-02-11 - 2023-05-14 #386592)
```

The correction can lead to undesired results though...\
Example:

```
180
  1 (2023-01-30: 2023-01-31 - 2023-02-01 #368123)
  3 (2023-01-31: 2023-01-01 - 2023-01-04 #368637)
```

The creation date is later than the given current expiration date, so the script corrects the calculation:

```
180
  1 (2023-01-30: 2023-01-31 - 2023-02-01 #368123)
  -27 (2023-01-31: 2023-01-01 - 2023-01-04 #368637)
```

The whole request doesn't make sense.\
We have to read the ticket to find out that there were two typos, both dates should be for month `2`.\
Humans entering data...

The script adds an extra check to see if the desired end date is before the creation date.\
If this is the case, it sets the duration to `-1`, as an indication that it's a weird request, and needs
manual inspection:

```
180
  1 (2023-01-30: 2023-01-31 - 2023-02-01 #368123)
  -1 (2023-01-31: 2023-01-01 - 2023-01-04 #368637)
```

G) Some tickets have incorrect dates.\
The current and desired expiration date are reversed, and there are incorrect months or other typos.\
Often the current expiration date of a previous license is used.\
With all these human errors, the calculated extension durations can't be fully trusted.\
Some incorrect calculations are hidden in plain sight and only discovered when analyzing the corresponding
internal requests.\
Others are obvious as the duration is a negative number.

Example:

```
17
  5 (2023-01-26: 2023-01-26 - 2023-01-31 #366724)
  -25 (2023-01-31: 2023-01-31 - 2023-01-06 #368650)
  16 (2023-07-13: 2023-07-15 - 2023-07-31 #429405)

```

The second extension has a negative duration.\
Were day and month swapped, and was it meant to be `2023-06-01`? (No)\
Or was the expiration date a typo, and meant to be `2023-02-06` for a 1 week extension? (Yes)\

The script doesn't try to correct this, as it has no way to determine what the correct date is.

H) Extensions can have an overlap, so the calculated extension durations can be too large.\
Example:

```
21
  28 (2023-07-10: 2023-07-10 - 2023-08-07 #427519)
  28 (2023-07-11: 2023-07-11 - 2023-08-08 #428175)
```

Maybe the first request was incorrect, and replaced by the second request?\
The script has no way of knowing, and doesn't try to correct this.

### SaaS trial extensions report logic

The script extracts the `GitLab.com namespace`, `Desired expiration date`, and the date the internal request
was created.

The difference between `Desired expiration date` and the creation date of the internal request is calculated
and stored as extension duration for a namespace.\
Give what we learned about human errors in the SM report, the calculation for SaaS trial extension duration
is very suspect...

