#!/usr/bin/env ruby
# frozen_string_literal: true

require 'csv'

CSV_FILE = 'ticket_exports.csv'
ANONYMIZE = true # If true, report replaces domains with numbers

def header(text)
  puts '-' * 50
  puts text
  puts '-' * 50
end

def extract_date(label, text)
  text.scan(/#{label}(\d{4}-\d{2}-\d{2})/).flatten[0]
end

def non_standard_extension(extensions)
  total_delta(extensions) > 31 || negative_delta(extensions)
end

def total_delta(extensions)
  extensions.sum { |extension| extension[:delta] }
end

def negative_delta(extensions)
  extensions.any? { |extension| extension[:delta].negative? }
end

class Trial

  attr_reader :csv_file_name
  attr_accessor :skipped, :sm_trials, :saas_trials

  def initialize(csv_file_name)
    @csv_file_name = csv_file_name
    @skipped = []
    @sm_trials = SMTrial.new
    @saas_trials = SaaSTrial.new
  end

  def scan
    # First 2 lines of Explore export are bogus, skip them
    CSV.parse(File.readlines(csv_file_name).drop(2).join, encoding: 'utf-8', headers: true) do |row|
      process row
    end
  end

  def report
    report_skipped
    report_sm
    report_saas
  end

  private

  def report_sm
    sm_trials.report
  end

  def report_saas
    saas_trials.report
  end

  def report_skipped
    header 'SKIPPED'
    skipped.each do |skip|
      puts "##{skip}"
    end
  end

  def process(row)
    case row['subject']
    when 'IR - SM - Extend an existing trial'
      sm_trials.process row['id'], row['created_at'], row['description']
    when 'IR - SaaS - Extend an existing trial'
      saas_trials.process row['id'], row['created_at'], row['description']
    else
      skipped << row['id']
    end
  end

end

class SMTrial

  attr_accessor :trials

  def initialize
    @trials = {}
  end

  def process(id, created_at, description)
    dom = get_domain(description)
    return unless dom
    created = Date.parse(created_at)
    old = get_current_expiration(description)
    new = get_desired_expiration(description)
    return unless old && new
    delta = calculate_delta created, old, new
    store id, dom, created, old, new, delta
  end

  def calculate_delta(created, old, new)
    old = Date.parse(old)
    new = Date.parse(new)
    if old < created
      if new > created
        # Use created as start if start < created
        (new - created).to_i
      else
        # Return -1 if both start and end < created
        -1
      end
    else
      (new - old).to_i
    end
  end

  def report
    header 'SELF-MANAGED'
    trials.each.with_index do |(domain, extensions), index|
      next unless non_standard_extension extensions
      report_non_standard_extensions index, domain, extensions
    end
  end

  private

  def store(id, dom, created, old, new, delta)
    trials[dom] = [] unless trials.key? dom
    values = trials[dom]
    values << { id:, created:, old:, new:, delta: }
  end

  def get_domain(text)
    domain = text.scan(/- Contact's email: .*@([\w.-]*)/)
    domain.flatten[0]
  end

  def get_current_expiration(text)
    extract_date('- Current license expiration date: ', text)
  end

  def get_desired_expiration(text)
    extract_date('- Desired expiration date: ', text)
  end

  def report_non_standard_extensions(index, domain, extensions)
    if ANONYMIZE
      puts index
    else
      puts domain
    end
    extensions.sort_by! { |k| k[:id] }
    extensions.each do |ext|
      puts "  #{ext[:delta]} (#{ext[:created]}: #{ext[:old]} - #{ext[:new]} ##{ext[:id]})"
    end
    puts
  end

end

class SaaSTrial

  attr_accessor :trials

  def initialize
    @trials = {}
  end

  def process(id, created_at, description)
    namespace = get_namespace(description)
    return unless namespace
    created = Date.parse(created_at)
    new = get_desired_expiration(description)
    return unless new
    delta = calculate_delta created, new
    store id, namespace, created, new, delta
  end

  def get_namespace(text)
    namespace = text.scan(/- GitLab.com namespace: ([\w.-]*)/)
    namespace.flatten[0]
  end

  def get_desired_expiration(text)
    extract_date('- Desired expiration date: ', text)
  end

  def calculate_delta(created,new)
    new = Date.parse(new)
    if new < created
      -1
    else
      (new - created).to_i
    end
  end

  def store(id, namespace, created, new, delta)
    trials[namespace] = [] unless trials.key? namespace
    values = trials[namespace]
    values << { id:, created:, new:, delta: }
  end

  def report
    header 'SaaS'
    trials.each.with_index do |(namespace, extensions), index|
      next unless non_standard_extension extensions
      report_non_standard_extensions index, namespace, extensions
    end
  end

  def report_non_standard_extensions(index, namespace, extensions)
    if ANONYMIZE
      puts index
    else
      puts namespace
    end
    extensions.sort_by! { |k| k[:id] }
    extensions.each do |ext|
      puts "  #{ext[:delta]} (#{ext[:created]}: #{ext[:new]} ##{ext[:id]})"
    end
    puts
  end

end

trials = Trial.new CSV_FILE
trials.scan
trials.report
